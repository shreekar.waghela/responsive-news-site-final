# The Brighton Times

This is clone of a trial website, which is fully responsive to screen size.
Following are the views for laptop, tab and mobile:

## Laptop View
![Laptop View](./images/full1.png)
![Laptop View](./images/full2.png)
![Laptop View](./images/full3.png)
![Laptop View](./images/full4.png)
## Tablet View
![Tablet View](./images/medium1.png)
![Tablet View](./images/medium2.png)
![Tablet View](./images/medium3.png)
## Mobile View
![Mobile View](./images/mobile1.png)
